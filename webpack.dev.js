const path = require('path')
const merge = require('webpack-merge');
const common = require('./webpack.common');

module.exports = merge(common, {
  output: {
    path: path.resolve(__dirname, 'public'),
    filename: 'build.js',
    crossOriginLoading: "anonymous"/*"use-credentials"*/
  },
  mode: 'development',
  devServer: {
    contentBase: path.join(__dirname, 'public'),
    compress: true,
    port: 5000,
    historyApiFallback: true
  }
});
