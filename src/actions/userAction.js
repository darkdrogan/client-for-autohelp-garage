import {ERROR_LOGIN, LOGOUT, RECEIVE_LOGIN, REQUEST_LOGIN} from './constants';

export function userLogout() {
  return {
    type: LOGOUT,
  };
}

export function attemptToLogin(login) {
  return {
    type: REQUEST_LOGIN,
    login: login,
  };
}

function successLogin(token) {
  return {
    type: RECEIVE_LOGIN,
    token,
  };
}

function errorLogin(error) {
  return {
    type: ERROR_LOGIN,
    error,
  };
}

export function fetchLogin(login, password) {
  return function(dispatch) {
    dispatch(attemptToLogin(login));
    return fetch('http://localhost:3000/login', {
      headers: ({'content-type': 'application/json'}),
      method: 'POST',
      body: JSON.stringify({login, password}),
    }).then((response) => response.json())
      .then(
        (json) => dispatch(successLogin(json.token)),
        (error) => dispatch(errorLogin(error))
      );
  };
}
