import {REQUEST_CARS, RECEIVE_CARS} from './constants';
import {loadState} from '../services/loadState';

function requestCars() {
  return {
    type: REQUEST_CARS,
  };
}

function receiveCars(json) {
  return {
    type: RECEIVE_CARS,
    items: json.map((item) => item),
  };
}

export function fetchCars() {
  return function(dispatch) {
    dispatch(requestCars());
    return fetch('http://localhost:3000/getCars', {
      headers: ({'Content-type': 'application/json'}),
      method: 'POST',
      body: JSON.stringify({'token': loadState().users.token}),
    })
      .then((response) => response.json())
      .then((jsonCars) => dispatch(receiveCars(jsonCars)))
      .catch((error) => console.log(error));
  };
}
