import React from 'react';
import {render} from 'react-dom';
import App from './components/App';
import {createStore, applyMiddleware} from 'redux';
import catalogApp from './reducers';
import {BrowserRouter} from 'react-router-dom';
import {Provider} from 'react-redux';
import thunkMiddleware from 'redux-thunk';
import {createLogger} from 'redux-logger';
import {fetchCars} from './actions/carsActions';
import './index.scss';
import firebase from 'firebase';

const config = {
  apiKey: 'AIzaSyC72ATyrkpx3qujdqB3ImZDx5FfngWmeqg',
  authDomain: 'autohelp-fed7a.firebaseapp.com',
  databaseURL: 'https://autohelp-fed7a.firebaseio.com',
  projectId: 'autohelp-fed7a',
  storageBucket: 'autohelp-fed7a.appspot.com',
  messagingSenderId: '495638519167',
};
firebase.initializeApp(config);

const loggerMiddleware = createLogger();
const initialState = {
  users: {
    token: '',
  },
};
const store = createStore(
  catalogApp,
  (localStorage['autohelp-catalog']) ? JSON.parse(localStorage['autohelp-catalog']) : initialState,
  applyMiddleware(
    thunkMiddleware,
    loggerMiddleware
  )
);


store.dispatch(fetchCars());

store.subscribe(() => {
  localStorage['autohelp-catalog'] = JSON.stringify(store.getState());
});

const MyComp = () =>
  <Provider store={store}>
    <BrowserRouter>
      <App/>
    </BrowserRouter>
  </Provider>;

render(<MyComp/>, document.getElementById('root'));
