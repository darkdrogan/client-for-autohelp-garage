export const loadState = () => {
  const serializedState = localStorage.getItem('autohelp-catalog');
  return serializedState === null
    // undefined don't work here
    ? {users: {token: ''}}
    : JSON.parse(serializedState);
};
