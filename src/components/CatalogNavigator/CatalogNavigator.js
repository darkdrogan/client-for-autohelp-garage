import React from 'react';
import PropTypes from 'prop-types';
import HistoryNavigation from './HistoryNavigation/HistoryNavigation';
import CatalogList from './CatalogList/CatalogList';

const firebase = require('firebase/app');

const CatalogNavigator = ({ match }) =>
  <div className='content'>
    <HistoryNavigation params={match.params}/>
    <CatalogList/>
  </div>

// TODO: поправь меня, если я ошибся со свойствами
CatalogNavigator.propTypes = {
  match: PropTypes.object,
};

export default CatalogNavigator;
