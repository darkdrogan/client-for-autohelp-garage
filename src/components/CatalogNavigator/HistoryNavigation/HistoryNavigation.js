import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

const HistoryNavigation = ({params}) => {
  let path = '/catalog';
  const getNewPass = (fragment) => `${path}/${params[fragment]}`;
  return (
    <div id='navigator'>
      <Link to='/catalog'>Catalog</Link>
      {
        Object.keys(params).map((param) =>
          // TODO: вот тут нужно указывать ключи для итерации. Почитай про это:
          // https://reactjs.org/docs/lists-and-keys.html
          <span key={param}>{'>>'}<Link to={path = getNewPass(param)}> {params[param]} </Link> </span>
        )
      }
    </div>
  );
};

// TODO: поправь меня, если я ошибся со свойствами
HistoryNavigation.propTypes = {
  params: PropTypes.object,
};

export default HistoryNavigation;
