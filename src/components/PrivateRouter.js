import React from 'react';
import {Route, Redirect} from 'react-router-dom';
// import {loadState} from '../services/loadState';
import firebase from 'firebase';

const PrivateRouter = ({component: Component, token, ...rest}) => {
  let user = firebase.auth().currentUser;
  return (
    < Route
      {...rest}
      render={(props) =>
        (user ?
        <Component {...props}/> :
        <Redirect to={{pathname: '/login', state: {pathTo: rest.location.pathname}}}/>)}
    />);
};

export default PrivateRouter;
