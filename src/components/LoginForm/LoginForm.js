import React from 'react';
import PropTypes from 'prop-types';
import {loadState} from '../../services/loadState';
import {Redirect, withRouter} from 'react-router-dom';
// what the fuck?! without this string it's all crushed
import firebase from 'firebase';
import './Login.scss'

const LoginForm = ({onClickLogIn, history}) => {
  let _login; let _pass;
  const {users} = loadState();
  const {login: user} = users;
  console.log(user);
  let currentUser = firebase.auth().currentUser;
  if (currentUser) {
    console.log(currentUser.email);
  }

  if (firebase.auth().currentUser) {
    return <Redirect to={'/catalog'}/>;
  } else {
    return (
      <div className='content'>
        <div className='login'>
          <form
            onFocus={(e) => e.preventDefault()}
          >
            <label>Login</label>
            <input id='loginInput' type='text' defaultValue={user} ref={(input) => _login = input} required/>
            <label>Password</label>
            <input type='password' ref={(input) => _pass = input} required/>
            <button type='submit' onClick={(event) => {
              event.preventDefault();
              onClickLogIn(_login.value, _pass.value);
              // _pass.value = ''
              firebase.auth().signInWithEmailAndPassword(_login.value, _pass.value).then(() => history.push('/catalog'));
              _pass.value = '';
            }}>Sign in
            </button>
          </form>
        </div>
      </div>
    );
  }
};

LoginForm.propTypes = {
  onClickLogIn: PropTypes.func.isRequired,
};

export default withRouter(LoginForm);
