import React from 'react';
import {Redirect, Route} from 'react-router-dom';
import LoginForm from './LoginForm';

const LoginBlock = ({component: Component, token, ...rest}) => {
  return (
    // TODO: вот тут не хватало параметра onClickLogIn, я заглушил
    <Route
      {...rest}
      render={
        (token) => (
          token === ''
            ? <LoginForm onClickLogIn={
              () => {
              }
            }/>
            : <Redirect to={'/catalog'}/>
        )
      }
    />
  );
};

export default LoginBlock;
