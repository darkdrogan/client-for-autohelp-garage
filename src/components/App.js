import React from 'react';
import PageNotFound from './pages/PageNotfound';
// import PageLogIn from './pages/PageLogIn';
import PageAdd from './pages/PageAdd'
import StartPage from './pages/StartPage';
import PageCatalog from './pages/PageCatalog';
import {Switch, Route} from 'react-router-dom';
import {PrivateRoute} from '../containers/PrivateRoute';
import PageAboutCar from './pages/PageAboutCar';
import LogInPageContainer from '../containers/LogInPageContainer';
import CatalogNavigator from './CatalogNavigator/CatalogNavigator';

const App = () =>
  <Switch>
    <Route exact path='/' component={StartPage}/>
    <Route path='/login' component={LogInPageContainer}/>
    <PrivateRoute path='/catalog' component={PageCatalog}/>
    <PrivateRoute path='/catalog/:brand/:model' component={PageCatalog}/>
    <PrivateRoute path='/add' component={PageAdd}/>
    {/* <Route path='/:filter' component={PageLogIn}/>*/}
    <PrivateRoute path='/car' component={PageAboutCar}/>
    <Route component={PageNotFound}/>
  </Switch>;

export default App;
