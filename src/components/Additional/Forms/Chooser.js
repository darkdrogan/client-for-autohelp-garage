import React from 'react'
import './Chooser.scss'

/**
 * @description this element for creating toggle, like a radio button with free params
 * @param1 {function} callback Callback for receive value on top
 * @param2 {array} items Array with values for creating name buttons and assignment values for callback
 */
class Chooser extends React.Component {
  constructor (props) {
    super(props);
    this.state = {};
    this.createArrayItems = this.createArrayItems.bind(this);
    this.handleOnClickChoice = this.handleOnClickChoice.bind(this);
  }

  createArrayItems () {
    const { items } = this.props;
    const array = [];
    items.map(item => array[ item ] = false)
    this.setState(array)
  }

  componentWillMount () {
    this.createArrayItems();
  }

  handleOnClickChoice (e) {
    e.preventDefault()
    const item = e.target.innerText;
    this.props.callback(item);
    this.setState((previousState) => {
      Object.keys(previousState).map(item => previousState[ item ] = false)
      return { ...previousState, [ item ]: true }
    })
  }

  render () {
    return (
      <div className='toggle-chooser'>
        {Object.keys(this.state).map(item => {
          return (<div onClick={this.handleOnClickChoice} key={item}
                       className={this.state[ item ] ? 'choose' : 'choice'}>{item}</div>)
        })}
      </div>
    )
  }
}

export default Chooser