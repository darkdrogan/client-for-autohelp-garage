import React from 'react';
import './Footer.scss'

const Footer = () =>
  <footer className={'footer'}>
    <div className={'footer-content'}>&#169;2018 DarkDrogan</div>
  </footer>;

export default Footer;
