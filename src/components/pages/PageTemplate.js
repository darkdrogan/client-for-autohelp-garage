import React from 'react';
import PropTypes from 'prop-types';
import Menu from '../Menu/Menu';
import Footer from '../Footer/Footer';

const PageTemplate = ({children}) =>
  <div className={'app'}>
    <Menu/>
    {children}
    <Footer/>
  </div>;

// TODO: поправь, если я ошибся с типами
PageTemplate.propTypes = {
  children: PropTypes.object,
};

export default PageTemplate;
