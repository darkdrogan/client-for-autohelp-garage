import React from 'react';
import PageTemplate from './PageTemplate';
import {Car} from '../../containers/Car';

const PageAboutCar = () =>
  <PageTemplate>
    <Car/>
  </PageTemplate>;

export default PageAboutCar;
