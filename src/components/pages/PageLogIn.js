import React from 'react';
import PageTemplate from './PageTemplate';
import {LoginFormContainer} from '../../containers/LoginFormContainer';
import {Redirect, Route} from 'react-router-dom';
import firebase from 'firebase'

const Login = () => {
  return (
    <PageTemplate>
      <LoginFormContainer/>
    </PageTemplate>
  );
};

//todo: поправить правильный редирект дальше, а то невозможно перейти по адресу... хотя
const PageLogIn = ({ history, pathTo, ...rest }) => {
  firebase.auth().onAuthStateChanged((user) => {
    if (user) {
      history.push(pathTo)
    }
  });

  return (
    < Route
      {...rest}
      render={
        (props) => (
          firebase.auth().currentUser
            ? <Redirect to={'/catalog'}/>
            : <Login {...props}/>
        )
      }
    />
  );
};

export default PageLogIn;
