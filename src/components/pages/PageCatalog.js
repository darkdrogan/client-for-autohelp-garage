import React from 'react';
import PageTemplate from './PageTemplate';
import CatalogNavigator from '../CatalogNavigator/CatalogNavigator';
import {Route, Switch} from 'react-router-dom';

const PageCatalog = () =>
  <PageTemplate>
    <Switch>
      <Route path='/catalog/:brand/:model/:year' component={CatalogNavigator}/>
      <Route path='/catalog/:brand/:model' component={CatalogNavigator}/>
      <Route path='/catalog/:brand' component={CatalogNavigator}/>
      <Route exact path='/catalog' component={CatalogNavigator}/>
    </Switch>
  </PageTemplate>;

export default PageCatalog;
