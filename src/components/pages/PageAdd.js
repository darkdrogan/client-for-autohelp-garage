import React from 'react'
import PageTemplate from './PageTemplate'
import Add from '../Add/Add';

const PageAdd = () =>
  <PageTemplate>
    <Add/>
  </PageTemplate>

export default PageAdd