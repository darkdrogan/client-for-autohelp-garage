import PageTemplate from './PageTemplate';
import React from 'react';
import {Link} from 'react-router-dom';

const StartPage = () =>
  <PageTemplate>
    <div className='content'>
      <div>This is start page</div>
      <Link to={'/login'}>Sign in</Link>
    </div>
  </PageTemplate>;

export default StartPage;
