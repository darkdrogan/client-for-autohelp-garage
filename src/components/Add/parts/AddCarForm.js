import React from 'react';
import Chooser from '../../Additional/Forms/Chooser'

class AddCarForm extends React.Component {
  constructor (props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleChooseGasolineType = this.handleChooseGasolineType.bind(this)
    this.handleChooseAddOwner = this.handleChooseAddOwner.bind(this)
  }

  handleSubmit (e) {
    e.preventDefault();
    let { _governmentNumber, _brand, _model, _year, _ed, _hp, _carBody, _engineModel, _gearboxType, _owner } = this.refs;
    const car = {
      governmentNumber: _governmentNumber.value,
      brand: _brand.value,
      model: _model.value,
      year: _year.value,
      engineDisplacement: _ed.value,
      horsePower: _hp.value,
      carBody: _carBody.value,
      engineModel: _engineModel.value,
      gearboxType: _gearboxType.value
    }

  }

  //todo: take type of gasoline here from Chooser element
  handleChooseGasolineType (item) {
    console.log('this is callback', item)
  }

  handleChooseAddOwner(item){
    console.log('owner', item)
  }

  render () {
    const items = [ 'petrol', 'diesel' ];
    return (
      <div className='l'>
        <label>Гос. номер</label>
        <input type='text' ref='_governmentNumber' required/>
        <label>
          Марка:
        </label>
        <input type='text' ref='_brand' required/>
        <label>
          Модель:
        </label>
        <input type='text' ref='_model' required/>
        <label>Год выпуска:</label>
        <input type="number" ref='_year' required/>
        <label>Объем двигателя</label>
        <input type='text' ref='_ed' required/>
        <label>Л.С.</label>
        <input type='text' ref='_hp' required/>
        <label>Тип топлива</label>
        <Chooser callback={this.handleChooseGasolineType} items={items}/>
        <label>Тип кузова</label>
        <input type='text' ref='_carBody' required/>
        <label>Модель двигателя</label>
        <input type='text' ref='_engineModel' required/>
        <label>Тип КПП</label>
        <input type='text' ref='_gearboxType' required/>
        <label>Владелец</label>
        <Chooser callback={this.handleChooseAddOwner} items={['добавить', 'не добавлять']}/>
        <button type='submit' onClick={this.handleSubmit}>post in console
        </button>
      </div>
    )
  }
};

export default AddCarForm