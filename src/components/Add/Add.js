import React from 'react'
import AddCarForm from './parts/AddCarForm';
import './Add.scss'

class Add extends React.Component {
  constructor () {
    super();
    this.handleChangeVINNumber = this.handleChangeVINNumber.bind(this);
    this.state = {
      vinEntered: false,
      vinExists: false
    }
  }

  handleChangeVINNumber (e) {
    const pceudoVinCars = [ {
      vin: 'aaaaaaaaaaaaaaaaa',
      brand: 'ford',
      owner: 'id1'
    } ];
    const { _vin } = this.refs;
    const vinNumber = _vin.value;
    vinNumber.length < 17 ?
      this.setState({
        vinEntered: false
      }) :
      this.setState({
        vinEntered: true
      })
    pceudoVinCars.find((item) => item === _vin.value) ?
      this.setState({
        vinExists: true
      }) :
      this.setState({
        vinExists: false
      })
  }

  render () {
    return (
      <div className='content'>
        <form ref='_form'>
          <label>
            VIN:
          </label>
          <input type='text' ref='_vin' onChange={this.handleChangeVINNumber} required/>
          <div className={this.state.vinEntered ? 'car-form-showed' : 'car-form'}>
            {this.state.vinExists ? <div>here cod car</div> : <AddCarForm/>}
          </div>
        </form>
      </div>
    )
  }
}

export default Add