import React from 'react';
import Navigation from './parts/Navigation';
import Search from './parts/Search';
import LoginLogoutMenu from '../../containers/LoginLogoutMenu';
import './Menu.scss'

const Menu = () =>
  <div id='menu' className={'menu'}>
    <header className={'menu-header'}>
      <Navigation/>
      <Search/>
      <LoginLogoutMenu/>
    </header>
  </div>;

export default Menu;
