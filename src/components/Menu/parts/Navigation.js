import React from 'react';
import {Link} from 'react-router-dom';

const Navigation = () =>
  <div className='menu-navigation'>
    <Link to='/catalog'>Каталог</Link>
    <Link to='/add'>Добавить</Link>
    <Link to='/'>Календарь</Link>
  </div>;

export default Navigation;
