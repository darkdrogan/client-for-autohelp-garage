import React from 'react';
import {Redirect} from 'react-router-dom';

class Search extends React.Component {
  constructor(props) {
    super(props);
    this.handleChangeSearchInput = this.handleChangeSearchInput.bind(this);
    this.handleSubmitSearch = this.handleSubmitSearch.bind(this);
  }

  state = {
    myValue: '',
    referrer: null,
  };

  handleChangeSearchInput(event) {
    event.preventDefault();
    if (event.target.value.search(/\W/gi) !== -1) {
      return state.myValue;
    }
    this.setState({myValue: event.target.value});
  }

  handleSubmitSearch(event) {
    event.preventDefault();
    this.setState((previousState) => ({...previousState, referrer: this.state.myValue}));
  }

  render() {
    if (this.state.referrer != null) {
      console.log(this.state.myValue);
      return <Redirect to='/car' vin={this.state.myValue}/>;
    }
    return (

      <div className='search'>
        <input value={this.state.myValue} onChange={this.handleChangeSearchInput}/>
        <button onClick={(e) => this.handleSubmitSearch(e)}>go</button>
      </div>
    );
  }
}
/* const Search = ({ onClick }) => {
  let input

  return (
    <div id='search'>
      <form onSubmit={e => {
        e.preventDefault
        onClick(input.value)
      }}>
        <input ref={node => {input = node}}/>
        <button type='submit'>go</button>
      </form>
    </div>
  )
}*/

export default Search;
