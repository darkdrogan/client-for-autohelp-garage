import React from 'react';
import PropTypes from 'prop-types';
import {Link, withRouter} from 'react-router-dom';
import './LoginMenu.scss';
import firebase from 'firebase'

const LoginMenu = ({onClickLogOut, history}) => {
  return (
    <div className='login-menu'>
      {firebase.auth().currentUser
        ? <span onClick={() => {
          firebase.auth().signOut().then(() => history.push("/login"))
        }}>Logout</span>
        : <Link to={'/catalog'}>Login</Link>}
    </div>
  );
};

LoginMenu.propTypes = {
  onClickLogOut: PropTypes.func,
};

export default withRouter(LoginMenu);
