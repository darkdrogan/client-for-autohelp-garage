import React from 'react';
import PropTypes from 'prop-types';

const InfoAboutCar = ({car}) =>
  <div className='content'>
    <div id='about-car'>{car['brand']} where {car['model']}
    </div>
  </div>;

InfoAboutCar.propTypes = {
  car: PropTypes.object.isRequired,
};

export default InfoAboutCar;
