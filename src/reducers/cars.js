import {RECEIVE_CARS, REQUEST_CARS} from '../actions/constants';

export function cars(state = {}, action) {
  switch (action.type) {
  case REQUEST_CARS:
    return {...state, isFetching: true};
  case RECEIVE_CARS:
    return {
      ...state, isFetching: false, items: action['items'],
    };
  default:
    return state;
  }
}
