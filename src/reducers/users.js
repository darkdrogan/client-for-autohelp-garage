import {
  ERROR_LOGIN,
  LOGOUT,
  RECEIVE_LOGIN,
  REQUEST_LOGIN,
} from '../actions/constants';

export function users(state = {login: '', token: ''}, action) {
  switch (action.type) {
  case REQUEST_LOGIN:
    return {...state, login: action['login'], token: '', error: '', isFetching: true};
  case RECEIVE_LOGIN:
    return {...state, token: action['token'], error: '', isFetching: false};
  case ERROR_LOGIN:
    return {...state, error: action['error'], token: '', isFetching: false};
  case LOGOUT:
    return {...state, token: '', isFetching: false};
  default:
    return state;
  }
}
