import {combineReducers} from 'redux';
import {cars} from './cars';
import {users} from './users';

const catalogApp = combineReducers({
  users,
  cars,
});

export default catalogApp;
