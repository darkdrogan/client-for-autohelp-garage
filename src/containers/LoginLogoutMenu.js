import {connect} from 'react-redux';
import {userLogout} from '../actions/userAction';
import LoginMenu from '../components/Menu/parts/LoginMenu';
import firebase from 'firebase';

const mapDispatchToProps = (dispatch) => {
  return {
    onClickLogOut: () => {
      firebase.auth().signOut().then(console.log('cool')).catch(error => console.log(error, error ));
      dispatch(userLogout());
    },
  };
};

export default connect(null, mapDispatchToProps)(LoginMenu);
