import {connect} from 'react-redux';
import LoginForm from '../components/LoginForm/LoginForm';
import {fetchLogin} from '../actions/userAction';
import {withRouter} from 'react-router-dom';

const mapDispatchToProps = (dispatch) => {
  return {
    onClickLogIn: (login, password) => {
      dispatch(fetchLogin(login, password));
    },
  };
};

export const LoginFormContainer = withRouter(connect(null, mapDispatchToProps)(LoginForm));
