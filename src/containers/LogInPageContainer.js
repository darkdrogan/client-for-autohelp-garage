import {connect} from 'react-redux';
import PageLogIn from '../components/pages/PageLogIn';

const mapStateToProps = (state, props, ...rest) => {
  return {
    token: state.users.token,
    pathTo: props.location.state.pathTo
  };
};

export default connect(mapStateToProps)(PageLogIn);
