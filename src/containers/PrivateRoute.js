import PrivateRouter from '../components/PrivateRouter';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';

const mapStateToProps = (state) => {
  return {token: state.users.token};
};

export const PrivateRoute = withRouter(connect(mapStateToProps)(PrivateRouter));
