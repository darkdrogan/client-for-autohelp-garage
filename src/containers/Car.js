import {connect} from 'react-redux';
import InfoAboutCar from '../components/InfoAboutCar/InfoAboutCar';
import {withRouter} from 'react-router-dom';
// import {fetchCars} from '../actions/carsActions';

const mapStateToProps = () => {
  return {car: {brand: 'ford', model: 'focus'}};
};

const mapDispatchToProps = () =>
  ({});

export const Car = withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(InfoAboutCar));
