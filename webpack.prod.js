const path = require('path');
const merge = require('webpack-merge');
const common = require('./webpack.common');

module.exports = merge(common, {
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'build.js',
    crossOriginLoading: 'anonymous',
  },
  mode: 'production',
});
